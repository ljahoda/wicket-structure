package cz.lax.wicket.structure;

import cz.lax.wicket.structure.config.*;
import cz.lax.wicket.structure.pages.HomePage;
import org.apache.wicket.*;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 07.11.13
 * Email: lukas@lukasjahoda.cz
 */
public class Application extends WebApplication {

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		getMarkupSettings().setStripComments(false);
		getMarkupSettings().setCompressWhitespace(false);
		getMarkupSettings().setStripWicketTags(true);

		getResourceSettings().setUseDefaultOnMissingResource(true);
		getResourceSettings().setThrowExceptionOnMissingResource(false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeConfigurationType getConfigurationType() {
		return Config.isDevel() ? RuntimeConfigurationType.DEVELOPMENT : RuntimeConfigurationType.DEPLOYMENT;
	}
}
